#!/bin/sh

set -e

envsubst < /etc/nginx/default.conf.tlp > /etc/nginx/conf.d/default.conf
nginx -g "deamon off;"